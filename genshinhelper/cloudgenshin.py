"""
@Project   : genshinhelper
@Author    : y1ndan
@Blog      : https://www.yindan.me
@GitHub    : https://github.com/y1ndan
"""

from .utils import request


def get_cloudgenshin_free_time(headers):
    url = 'https://api-cloudgame.mihoyo.com/hk4e_cg_cn/wallet/wallet/get'
    return request('get', url, headers=headers).json()


def get_cloudgenshin_notifications(headers):
    url = 'https://api-cloudgame.mihoyo.com/hk4e_cg_cn/gamer/api/listNotifications'
    params = {
        'is_sort': 'true',
        'status': 'NotificationStatusUnread',
        'type': 'NotificationTypePopup'
    }
    return request('get', url, params=params, headers=headers).json()


def ack_cloudgenshin_notification(headers, notification_id):
    url = 'https://api-cloudgame.mihoyo.com/hk4e_cg_cn/gamer/api/ackNotification'
    data = {
        'id': notification_id
    }
    return request('post', url, json=data, headers=headers).json()
